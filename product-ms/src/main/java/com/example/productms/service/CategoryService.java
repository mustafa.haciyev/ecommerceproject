package com.example.productms.service;


import com.example.productms.entity.Category;

import java.util.List;

public interface CategoryService {



    Category editCategory(Category category);

    void deleteCategory(Long id);

//    Page<Category> getRequestFilters(int page, int limit, String categoryName, Sort.Direction sortType);

    Category addCategory(Category category);

    void findAll();

    List<Category> getAllCategories();
}
