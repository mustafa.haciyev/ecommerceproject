package com.example.productms.entity;

public enum UserAuthority {

    ADMIN,
    USER
}
