package com.example.orderms.controller;

import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.entity.OrderStatus;
import com.example.orderms.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/{userId}")
    public OrderResponseDto createOrder(@PathVariable Long userId, @RequestBody OrderRequestDto orderRequestDto) {
        orderService.createOrder(userId,orderRequestDto);
        return OrderResponseDto.builder()
                .id(userId)
                .orderStatus(orderRequestDto.getOrderStatus())
                .shippingAddress(orderRequestDto.getShippingAddress())
                .totalPrice(orderRequestDto.getTotalPrice())
                .build();
    }

}

