package com.example.orderms.controller;


import com.example.orderms.dto.request.OrderItemRequestDto;
import com.example.orderms.dto.response.OrderItemresponseDto;
import com.example.orderms.service.OrderItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/orderItems")
@RequiredArgsConstructor
public class OrderItemController {

  private final OrderItemService orderItemService;

  @PostMapping("/{productId}/{orderId}")
  public OrderItemresponseDto createOrderItem(@PathVariable Long orderId,@PathVariable Long productId, @RequestBody OrderItemRequestDto orderItemRequestDto) {
      orderItemService.createOrderItem(productId,orderId,orderItemRequestDto);
      return OrderItemresponseDto.builder()
//              .orderId(orderItemRequestDto.getOrderId())
//              .productName(String.valueOf(orderItemRequestDto.getProductId()))
              .price(orderItemRequestDto.getPrice())
              .quantity(orderItemRequestDto.getQuantity())
              .build();
  }
}
