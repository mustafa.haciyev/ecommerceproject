package com.example.orderms.service;

import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.entity.Order;

import java.util.List;

public interface OrderService {


    void createOrder(Long userId, OrderRequestDto orderRequestDto);
}
