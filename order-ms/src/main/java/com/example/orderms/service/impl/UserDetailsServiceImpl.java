package com.example.orderms.service.impl;
import com.example.orderms.entity.Users;
import com.example.orderms.repository.UserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        log.info("Username is : {}", username);

        Users users = userRepo.findByName(username).orElseThrow(() -> new RuntimeException("User not found"));
        return User.builder()
                .username(users.getName())
                .password(users.getPassword())
                .authorities(users.getAuthorities().stream()
                        .map(authority -> new SimpleGrantedAuthority(authority.getUserAuthority()
                                .name()))
                        .collect(Collectors.toSet()))
                .build();

    }

}
