package com.example.orderms.service;

import com.example.orderms.dto.request.OrderItemRequestDto;
import com.example.orderms.entity.Order;
import com.example.orderms.entity.OrderItem;
import com.example.orderms.entity.Product;
import com.example.orderms.mapstruct.OrderMapper;
import com.example.orderms.repository.OrderItemRepository;
import com.example.orderms.repository.OrderRepository;
import com.example.orderms.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderItemServiceImpl implements OrderItemService {


    private final OrderItemRepository orderItemRepository;
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;
    private final OrderMapper orderMapper;

    @Override
    public List<OrderItem> getAllOrderItems() {
        return orderItemRepository.findAll();
    }

    @Override
    public OrderItem getOrderItemById(Long id) {
        return orderItemRepository.findById(id).orElse(null);
    }

    @Override
    public OrderItem saveOrderItem(OrderItem orderItem) {
        return orderItemRepository.save(orderItem);
    }

    @Override
    public void deleteOrderItem(Long id) {
        orderItemRepository.deleteById(id);
    }

    @Override
    public void createOrderItem(Long orderId,Long productId, OrderItemRequestDto orderItemRequestDto) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found"));
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));
        OrderItem orderItem = orderMapper.mapRequestToOrderItem(orderItemRequestDto);
        orderItem.setOrder(order);
        orderItem.setPrice(orderItemRequestDto.getPrice());
        orderItem.setProduct(product);
        orderItem.setQuantity(orderItemRequestDto.getQuantity());
        orderItemRepository.save(orderItem);
    }
}
