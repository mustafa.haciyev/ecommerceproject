package com.example.orderms.service;


import com.example.orderms.dto.request.OrderItemRequestDto;
import com.example.orderms.entity.OrderItem;

import java.util.List;

public interface OrderItemService {
    List<OrderItem> getAllOrderItems();
    OrderItem getOrderItemById(Long id);
    OrderItem saveOrderItem(OrderItem orderItem);
    void deleteOrderItem(Long id);

    void createOrderItem(Long orderId, Long productId,OrderItemRequestDto orderItemRequestDto);
}
