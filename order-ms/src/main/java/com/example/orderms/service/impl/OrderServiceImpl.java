package com.example.orderms.service.impl;

import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.entity.Order;
import com.example.orderms.entity.OrderItem;
import com.example.orderms.entity.OrderStatus;
import com.example.orderms.entity.Users;
import com.example.orderms.mapstruct.OrderMapper;
import com.example.orderms.repository.OrderRepository;
import com.example.orderms.repository.UserRepo;
import com.example.orderms.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final UserRepo userRepo;
    private final OrderMapper orderMapper;
    @Override
    public void createOrder(Long userId, OrderRequestDto orderRequestDto) {
        Users users = userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));

     Order order = orderMapper.mapRequestToOrder(orderRequestDto);
     order.setUsers(users);
     order.setTotalPrice(orderRequestDto.getTotalPrice());
     order.setShippingAddress(orderRequestDto.getShippingAddress());
     order.setOrderStatus(orderRequestDto.getOrderStatus());
     orderRepository.save(order);
    }
}

