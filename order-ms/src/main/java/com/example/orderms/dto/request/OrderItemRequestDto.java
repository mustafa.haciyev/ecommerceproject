package com.example.orderms.dto.request;

import com.example.orderms.entity.Order;
import com.example.orderms.entity.Product;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderItemRequestDto {
//    Long orderId;
//    Long productId; // Sadece tamsayı tipinde ürün ID'sini içeriyor
    double price;
    int quantity;
}
