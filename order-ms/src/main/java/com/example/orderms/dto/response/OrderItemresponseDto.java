package com.example.orderms.dto.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderItemresponseDto {
//    Long orderId;
//    String productName; // Sadece tamsayı tipinde ürün ID'sini içeriyor
    double price;
    int quantity;
}
