package com.example.orderms.dto.response;

import com.example.orderms.entity.OrderStatus;
import com.example.orderms.entity.Users;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderResponseDto {

    private Long id;
    private BigDecimal totalPrice;
    private String shippingAddress;
    private OrderStatus orderStatus;


}
