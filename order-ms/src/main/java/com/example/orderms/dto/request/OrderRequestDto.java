package com.example.orderms.dto.request;

import com.example.orderms.entity.OrderItem;
import com.example.orderms.entity.OrderStatus;
import jakarta.persistence.criteria.Order;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrderRequestDto {
    BigDecimal totalPrice;
    private List<OrderItemRequestDto> orderItems;
    private String shippingAddress;
    OrderStatus orderStatus;

//    public void setTotalPrice(BigDecimal totalPrice) {
//        this.totalPrice = totalPrice;
//    }



}
