package com.example.orderms.entity;

public enum UserAuthority {

    ADMIN,
    USER
}
