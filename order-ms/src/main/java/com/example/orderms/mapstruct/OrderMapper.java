package com.example.orderms.mapstruct;

import com.example.orderms.dto.request.OrderItemRequestDto;
import com.example.orderms.dto.request.OrderRequestDto;
import com.example.orderms.dto.response.OrderResponseDto;
import com.example.orderms.entity.Order;
import com.example.orderms.entity.OrderItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface OrderMapper {
//    @Mapping(target = "users", ignore = true) // Bu kısmı ekle
    Order mapRequestToOrder(OrderRequestDto orderRequestDto);

    OrderResponseDto mapEntitytoResponse(Order order);

    OrderItem mapRequestToOrderItem(OrderItemRequestDto orderItemRequestDto);

}
