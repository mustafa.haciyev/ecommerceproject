package com.example.orderms.config;

import com.example.auth.config.GeneralConfig;
import com.example.auth.config.JwtService;
import com.example.auth.config.JwtTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Import({JwtTokenService.class, JwtService.class})
public class SecurityConfig extends GeneralConfig {
    public SecurityConfig(UserDetailsService userDetailsService, JwtTokenService jwtTokenService) {
        super(userDetailsService, jwtTokenService);
    }

    @Bean
    @Override
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> auth
                .requestMatchers("/orderItems/**").authenticated() // Allow only authenticated users to access POST endpoints for OrderItems
                .requestMatchers("/orders/**").authenticated()

        );
        return super.filterChain(http);
    }

}
