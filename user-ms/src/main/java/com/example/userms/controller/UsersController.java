package com.example.userms.controller;

import com.example.auth.config.JwtService;
import com.example.userms.dto.JwtResponseDto;
import com.example.userms.dto.LoginRequestDto;
import com.example.userms.dto.RegisterRequestDto;
import com.example.userms.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UsersController {

    private final UsersService usersService;
    private final JwtService jwtService;


    @PostMapping("/register")
    public ResponseEntity<JwtResponseDto> register(@RequestBody RegisterRequestDto registerRequestDto) {
        return new ResponseEntity<JwtResponseDto>(usersService.register(registerRequestDto), HttpStatus.OK);
    }
//
//    @PostMapping("/login")
//    public ResponseEntity<JwtResponseDto> login(@RequestBody LoginRequestDto loginRequestDto ) {
//        JwtResponseDto jwtResponseDto = usersService.login(loginRequestDto);
//        if (jwtResponseDto != null) {
//            return ResponseEntity.ok(jwtResponseDto);
//        } else {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
//        }
//
//    }

}
