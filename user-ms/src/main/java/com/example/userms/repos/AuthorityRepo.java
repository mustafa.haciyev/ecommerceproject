package com.example.userms.repos;

import com.example.userms.entity.Authority;
import com.example.userms.entity.UserAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepo extends JpaRepository<Authority,Long> {
    Optional<Authority> findByUserAuthority(UserAuthority userAuthority);
}
