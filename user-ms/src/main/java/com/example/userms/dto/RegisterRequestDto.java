package com.example.userms.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterRequestDto {
    String username;
    String password;
    String repeatPassword;
    String email;
    String address;
    String identity_no;
    List<String> authorities;
}
