package com.example.userms.config;

import com.example.auth.config.GeneralConfig;
import com.example.auth.config.JwtService;
import com.example.auth.config.JwtTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@Import({JwtTokenService.class, JwtService.class})
public class SecurityConfig extends GeneralConfig {
    public SecurityConfig(UserDetailsService userDetailsService, JwtTokenService jwtTokenService) {
        super(userDetailsService, jwtTokenService);
    }

    @Bean
    @Override
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {

        return super.filterChain(http);
    }

}
