package com.example.userms.service;


import com.example.userms.dto.JwtResponseDto;
import com.example.userms.dto.LoginRequestDto;
import com.example.userms.dto.RegisterRequestDto;

public interface UsersService {

    JwtResponseDto register(RegisterRequestDto registerRequestDto);


//    JwtResponseDto login(LoginRequestDto loginRequestDto);

}
