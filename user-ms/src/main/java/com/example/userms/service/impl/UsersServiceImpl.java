package com.example.userms.service.impl;


import com.example.auth.config.JwtService;
import com.example.userms.dto.JwtResponseDto;
import com.example.userms.dto.LoginRequestDto;
import com.example.userms.dto.RegisterRequestDto;
import com.example.userms.entity.Authority;
import com.example.userms.entity.UserAuthority;
import com.example.userms.entity.Users;
import com.example.userms.repos.AuthorityRepo;
import com.example.userms.repos.UserRepo;
import com.example.userms.service.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthorityRepo authorityRepo;

    private final UserDetailsService userDetailsService;

//    @Override
//    public JwtResponseDto login(LoginRequestDto loginRequestDto) {
//        Users user = userRepo.findByName(loginRequestDto.getUsername())
//                .orElseThrow(() -> new RuntimeException("User not found"));
//
//        if (passwordEncoder.matches(loginRequestDto.getPassword(), user.getPassword())) {
//            UserDetails userDetails = userDetailsService.loadUserByUsername(user.getName());
//            String token = jwtService.generateToken(user);
//            return new JwtResponseDto(token);
//        }
//
//        throw new RuntimeException("Authentication failed");
//    }


    public JwtResponseDto register(RegisterRequestDto registerRequestDto) {

        userRepo.findByName(registerRequestDto.getUsername())
                .ifPresent(users -> {
                    throw new RuntimeException("User not found");
                });
        if (!registerRequestDto.getPassword().equals(registerRequestDto.getRepeatPassword())) {
            throw new RuntimeException("Passwords are different");
        }
        List<Authority> authorityList = new ArrayList<>();

        registerRequestDto.getAuthorities().forEach(s -> {
            UserAuthority userAuthority = UserAuthority.valueOf(s);
            authorityList.add(Authority.builder()
                    .userAuthority(userAuthority)
                    .build());
        });

        Users user = Users.builder()
                .name(registerRequestDto.getUsername())
                .password(passwordEncoder.encode(registerRequestDto.getPassword()))
                .address(registerRequestDto.getAddress())
                .email(registerRequestDto.getEmail())
                .identity_no(registerRequestDto.getIdentity_no())
                .authorities(authorityList)
                .build();
        Users saveUser = userRepo.save(user);

        Set<SimpleGrantedAuthority> authorities = saveUser.getAuthorities().stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getUserAuthority().name()))
                .collect(Collectors.toSet());

        UserDetails userDetails = org.springframework.security.core.userdetails.User.builder()
                .username(saveUser.getName())
                .password(saveUser.getPassword())
                .authorities(authorities)
                .build();

        String token = jwtService.generateToken((org.springframework.security.core.userdetails.User) userDetails);
        return JwtResponseDto.builder()
                .jwt(token)
                .build();
    }
}




