package com.example.userms.entity;

public enum UserAuthority {

    ADMIN,
    USER
}
